# Exams Template

Produz um exame em pdf a partir de um ficheiro .md (Markdown)

## Pré-requisitos

* pandoc e latex instalado

## Instalação

Em Mac, copiar os ficheiros exam\_template.latex e logo\_ulht.png para ~/.pandoc/templates

## Utilização

pandoc --template=exam\_template.latex -fmarkdown-implicit\_figures sample.md -o sample.pdf

## FAQ

### Como colocar comentários?

<!---
your comment goes here
and here
-->

### Como forçar a quebra de página?

\newpage

### Como reduzir o tamanho da fonte para blocos de código?

* Copiar o ficheiro make-code-footnotesize.tex para a pasta onde está o .md
* Correr o comando: pandoc --template=exam\_template.latex -fmarkdown-implicit\_figures sample.md -o sample.pdf -H make-code-footnotesize.tex --highlight-style=tango