---
subject: Algoritmos
period: 2015-2016 2º semestre
testId: Teste A
title: Exame 2ª época
date: 8 Setembro 2016
duration: 1h20
draftpage: true
geometry: margin=1in
...


**Notas prévias**

* Utilize a folha de rascunho fornecida para preparar a resposta. Nestas folhas deverá escrever apenas a resposta definitiva de forma legível e sem rasuras.
* Preencha o seu número de aluno em todas as páginas.


# Primeira questão

## Indique ...

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

## Justifique que ...

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

# Segunda questão

## Como funciona ...

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

\newpage

# Questão com imagem

![](image.png)

## Que alterações ...

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

